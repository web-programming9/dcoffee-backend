import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    length: '150',
  })
  address: string;

  @Column({
    unique: true,
    length: '9',
  })
  tel: string;

  @Column({
    unique: true,
    length: '64',
  })
  email: string;

  @Column({
    length: '32',
  })
  position: string;

  @Column({
    type: 'float',
  })
  hourly_wage: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
