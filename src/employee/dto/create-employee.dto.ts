import { IsNotEmpty, Length, IsEmail, Matches, Min } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  @Length(3, 150)
  address: string;

  @IsNotEmpty()
  @Length(9, 9)
  tel: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(3, 32)
  position: string;

  @IsNotEmpty()
  @Min(0)
  hourly_wage: number;
}
