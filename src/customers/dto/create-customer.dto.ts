import { IsNotEmpty, Min, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  @Length(9, 9)
  tel: string;

  @IsNotEmpty()
  @Min(0)
  point: number;
}
