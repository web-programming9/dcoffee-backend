import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    unique: true,
    length: '9',
  })
  tel: string;

  @Column({
    type: 'float',
  })
  point: number;

  @CreateDateColumn()
  start_date: Date;

  @OneToMany(() => Order, (order) => order.customer)
  orders: Order[];

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
